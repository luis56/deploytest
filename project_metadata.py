NAME = 'test_deploy_php'

VERSION = '0.0.5'

FABRIC_CONFIGURATION = {
    'qa': {
        'root': '/home/projects/test_deploy_php',
        'hosts': ['root@198.199.126.83'],
        'docker_compose_file': 'docker-compose.yml',
        'migrate': True,
    },
}

