import os, time
from fabric.api import run, env
from fabric.colors import green
from project_metadata import FABRIC_CONFIGURATION

"""
Preparing the configuration
"""
deploy_environment = os.getenv('DEPLOY_ENV', None)

if not deploy_environment or not FABRIC_CONFIGURATION.get(deploy_environment, None):
    raise EnvironmentError('Define the DEPLOY_ENV environment variable')
fabric_conf = FABRIC_CONFIGURATION.get(deploy_environment)

# Configuring Fabric
env.hosts = fabric_conf['hosts']
# env.passwords = {'root@198.199.126.83': 'Amsterdam2019'}
docker_compose_file = fabric_conf['docker_compose_file']
project_root = fabric_conf['root']
migrate = fabric_conf['migrate']

"""
Deploying
"""


def deploy(tag_or_commit):
    """
    Deploy the given tag or commit on the servers
    :param tag_or_commit:
    :return:
    """
    print(green('> Deploying tag %s on %s' % (tag_or_commit, project_root,)))
    run('cd %s && git reset --hard HEAD' % project_root)
    run('cd %s && git fetch origin' % project_root)
    run('cd %s && git checkout %s' % (project_root, tag_or_commit,))

    print(green('> Building new images on %s' % project_root))
    run('cd %s && docker-compose -f %s build' % (project_root, docker_compose_file,))
    run('cd %s && docker-compose -f %s stop' % (project_root, docker_compose_file,))
    run('cd %s && docker-compose -f %s rm -f' % (project_root, docker_compose_file,))
    run('cd %s && docker-compose -f %s up -d' % (project_root, docker_compose_file,))

    print(green('Outputting status of containers..'))
    time.sleep(4)
    run('cd %s && docker-compose -f %s ps' % (project_root, docker_compose_file,))

    if migrate:
        # Put the migrate script here
        pass

